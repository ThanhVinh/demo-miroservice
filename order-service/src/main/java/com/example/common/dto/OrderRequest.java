package com.example.common.dto;

import com.example.common.constant.InventoryStatus;
import com.example.common.constant.OrderStatus;
import com.example.common.constant.PaymentStatus;

import java.util.HashSet;
import java.util.Set;
public class OrderRequest {
    private Long orderId;
    private Long userId;
    private Double totalPrice;
    private String shipPhone;
    private String shipAddress;
    private OrderStatus status;
    private PaymentStatus paymentStatus;
    private InventoryStatus inventoryStatus;
    private Set<OrderDetailDto> orderDetailSet = new HashSet<>();

    public OrderRequest() {
    }

    public OrderRequest(Long orderId, Long userId, Double totalPrice, String shipPhone, String shipAddress, OrderStatus status, PaymentStatus paymentStatus, InventoryStatus inventoryStatus, Set<OrderDetailDto> orderDetailSet) {
        this.orderId = orderId;
        this.userId = userId;
        this.totalPrice = totalPrice;
        this.shipPhone = shipPhone;
        this.shipAddress = shipAddress;
        this.status = status;
        this.paymentStatus = paymentStatus;
        this.inventoryStatus = inventoryStatus;
        this.orderDetailSet = orderDetailSet;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getShipPhone() {
        return shipPhone;
    }

    public void setShipPhone(String shipPhone) {
        this.shipPhone = shipPhone;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public InventoryStatus getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(InventoryStatus inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public Set<OrderDetailDto> getOrderDetailSet() {
        return orderDetailSet;
    }

    public void setOrderDetailSet(Set<OrderDetailDto> orderDetailSet) {
        this.orderDetailSet = orderDetailSet;
    }

    @Override
    public String toString() {
        return "OrderRequest{" +
                "orderId=" + orderId +
                ", userId=" + userId +
                ", totalPrice=" + totalPrice +
                ", shipPhone='" + shipPhone + '\'' +
                ", shipAddress='" + shipAddress + '\'' +
                ", status=" + status +
                ", paymentStatus=" + paymentStatus +
                ", inventoryStatus=" + inventoryStatus +
                ", orderDetailSet=" + orderDetailSet +
                '}';
    }
}
