package com.example.common.dto;

import java.util.HashSet;
import java.util.Set;


public class InventoryDto {
    private Long orderId;
    private Set<OrderDetailDto> set = new HashSet<>();

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Set<OrderDetailDto> getSet() {
        return set;
    }

    public void setSet(Set<OrderDetailDto> set) {
        this.set = set;
    }

    public InventoryDto() {
    }

    public InventoryDto(Long orderId, Set<OrderDetailDto> set) {
        this.orderId = orderId;
        this.set = set;
    }

    @Override
    public String toString() {
        return "InventoryDto{" +
                "orderId=" + orderId +
                ", set=" + set +
                '}';
    }
}
