package com.example.orderservice.controller;


import com.example.common.dto.OrderRequest;
import com.example.common.event.OrderEvent;
import com.example.orderservice.entity.Order;
import com.example.orderservice.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/order")
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);
    private ObjectMapper mapper = new ObjectMapper();
    @PostMapping("/placeOrder")
    public ResponseEntity<?> sendMessage(@RequestBody OrderRequest orderRequest) throws JsonProcessingException {
        OrderRequest order = orderService.placeOrder(orderRequest);
        OrderEvent orderEvent = new OrderEvent(order, order.getStatus());
        System.out.println("--------star order event system.out--------");
        LOG.info("--------star order event LOG--------");
        System.out.println("order event : "+ orderEvent.toString());
        rabbitTemplate.convertAndSend("payment-exchange","payment.routing.test", mapper.writeValueAsString(orderEvent));
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAllOrder(){
        return new ResponseEntity<>(orderService.search(), HttpStatus.OK);
    }

}
