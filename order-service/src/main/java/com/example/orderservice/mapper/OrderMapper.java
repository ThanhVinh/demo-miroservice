package com.example.orderservice.mapper;

import com.example.common.dto.OrderRequest;
import com.example.orderservice.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface OrderMapper {
    Order dtoToEntity(OrderRequest orderRequest);
    OrderRequest entityToDto(Order order);
}
