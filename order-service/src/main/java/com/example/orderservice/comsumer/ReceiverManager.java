package com.example.orderservice.comsumer;

import com.example.common.constant.OrderStatus;
import com.example.common.event.OrderEvent;
import com.example.orderservice.entity.Order;
import com.example.orderservice.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceiverManager implements RabbitListenerConfigurer {
    @Autowired
    private OrderService orderService;

    ObjectMapper mapper = new ObjectMapper();


    @RabbitListener(queues = "order-queue")
    public void receiveMessage(String message) throws JsonProcessingException {
        OrderEvent orderEvent = mapper.readValue(message, OrderEvent.class);
        System.out.println("update order status, orderEvent :"+ orderEvent.toString());
        orderService.updateStatusPlaceOrder(orderEvent.getOrderRequest().getOrderId(), orderEvent.getOrderStatus(),orderEvent.getOrderRequest().getPaymentStatus(), orderEvent.getOrderRequest().getInventoryStatus());
    }

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }
}
