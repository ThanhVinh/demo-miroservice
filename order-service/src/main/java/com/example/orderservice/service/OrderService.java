package com.example.orderservice.service;


import com.example.common.constant.InventoryStatus;
import com.example.common.constant.OrderStatus;
import com.example.common.constant.PaymentStatus;
import com.example.common.dto.OrderRequest;
import com.example.orderservice.entity.Order;

import java.util.List;

public interface OrderService {
    OrderRequest placeOrder(OrderRequest orderRequest);
    List<Order> search();
    Order findById(Long id);
    void updateStatusPlaceOrder(Long orderId, OrderStatus orderStatus, PaymentStatus paymentStatus, InventoryStatus inventoryStatus);
}
