package com.example.orderservice.service.impl;

import com.example.common.constant.InventoryStatus;
import com.example.common.constant.OrderStatus;
import com.example.common.constant.PaymentStatus;
import com.example.common.dto.OrderRequest;
import com.example.orderservice.controller.OrderController;
import com.example.orderservice.entity.Order;
import com.example.orderservice.entity.OrderDetail;
import com.example.orderservice.mapper.OrderMapper;
import com.example.orderservice.repository.OrderRepository;
import com.example.orderservice.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service

public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderMapper orderMapper;
    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Transactional
    @Override
    public OrderRequest placeOrder(OrderRequest orderDto) {
        Order order = new Order();
        order.setShipAddress(orderDto.getShipAddress());
        order.setTotalPrice(orderDto.getTotalPrice());
        order.setUserId(orderDto.getUserId());
        order.setShipPhone(orderDto.getShipPhone());
        order.setStatus(OrderStatus.ORDER_CREATED);
        order.getOrderDetailSet().addAll(orderDto.getOrderDetailSet().stream().map(orderDetailDto ->{
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrder(order);
            orderDetail.setQuantity(orderDetailDto.getQuantity());
            orderDetail.setUnitPrice(orderDetailDto.getUnitPrice());
            orderDetail.setProductName(orderDetailDto.getProductName());
            orderDetail.setProductId(orderDetailDto.getProductId());
            return orderDetail;
        }).collect(Collectors.toList()));
        return Optional.ofNullable(orderRepository.save(order)).map(orderMapper::entityToDto).orElse(null);

    }

    @Override
    public List<Order> search() {
        return orderRepository.findAll();
    }

    @Override
    public Order findById(Long id) {
        return null;
    }

    @Override
    @Transactional
    public void updateStatusPlaceOrder(Long orderId, OrderStatus orderStatus, PaymentStatus paymentStatus, InventoryStatus inventoryStatus) {
        this.orderRepository.findById(orderId)
                .ifPresent(ub -> {
                    ub.setStatus(orderStatus);
                    ub.setInventoryStatus(inventoryStatus);
                    ub.setPaymentStatus(paymentStatus);

                });
    }


}
