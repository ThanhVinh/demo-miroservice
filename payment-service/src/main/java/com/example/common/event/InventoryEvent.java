package com.example.common.event;

import com.example.common.Event;
import com.example.common.constant.InventoryStatus;
import com.example.common.constant.OrderStatus;
import com.example.common.dto.InventoryDto;
import com.example.common.dto.OrderRequest;

import java.util.Date;
import java.util.UUID;

public class InventoryEvent implements Event {

    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private InventoryDto inventoryDto;
    private InventoryStatus inventoryStatus;

    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return date;
    }

    public InventoryDto getInventoryDto() {
        return inventoryDto;
    }

    public void setInventoryDto(InventoryDto inventoryDto) {
        this.inventoryDto = inventoryDto;
    }

    public InventoryStatus getInventoryStatus() {
        return inventoryStatus;
    }

    public void setInventoryStatus(InventoryStatus inventoryStatus) {
        this.inventoryStatus = inventoryStatus;
    }

    public InventoryEvent() {
    }

    public InventoryEvent(InventoryDto inventoryDto, InventoryStatus inventoryStatus) {
        this.inventoryDto = inventoryDto;
        this.inventoryStatus = inventoryStatus;
    }

    @Override
    public String toString() {
        return "InventoryEvent{" +
                "eventId=" + eventId +
                ", date=" + date +
                ", inventoryDto=" + inventoryDto +
                ", inventoryStatus=" + inventoryStatus +
                '}';
    }
}
