package com.example.common.event;

import com.example.common.Event;
import com.example.common.constant.PaymentStatus;
import com.example.common.dto.PaymentDto;

import java.util.Date;
import java.util.UUID;

public class PaymentEvent implements Event {
    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private PaymentDto payment;
    private PaymentStatus paymentStatus;

    public PaymentEvent() {
    }

    public PaymentEvent(PaymentDto payment, PaymentStatus paymentStatus) {
        this.payment = payment;
        this.paymentStatus = paymentStatus;
    }

    @Override
    public UUID getEventId() {
        return eventId;
    }

    @Override
    public Date getDate() {
        return date;
    }

    public PaymentDto getPayment() {
        return payment;
    }

    public void setPayment(PaymentDto payment) {
        this.payment = payment;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Override
    public String toString() {
        return "PaymentEvent{" +
                "eventId=" + eventId +
                ", date=" + date +
                ", payment=" + payment +
                ", paymentStatus=" + paymentStatus +
                '}';
    }
}
