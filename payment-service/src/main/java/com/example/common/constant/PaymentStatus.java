package com.example.common.constant;

public enum PaymentStatus {
    RESERVED, REJECTED, REFUND
}
