package com.example.common.constant;

public enum InventoryStatus {
    RESERVED,
    REJECTED;
}
