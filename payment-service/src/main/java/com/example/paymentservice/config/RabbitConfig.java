package com.example.paymentservice.config;

import com.example.paymentservice.comsumer.ReceiverManager;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    private final String queueName = "payment-queue";
    public static final String topicExchangeName = "payment-exchange";
    //private final String queueName = "spring-boot-rabbitmq-response";
    //defined container
    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    //method is registered as a message listener in the container
    @Bean
    MessageListenerAdapter listenerAdapter(ReceiverManager receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

    // method creates an AMQP queue
    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    //method creates a topic exchange
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

//    @Bean
//    FanoutExchange exchange() {
//        return new FanoutExchange(topicExchangeName);
//    }

    //method binds these two together (queue and exchange), defining the behavior that occurs when RabbitTemplate publishes to an exchange.
    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("payment.routing.#");
    }

//    @Bean
//    Binding binding(Queue queue, FanoutExchange exchange) {
//        return BindingBuilder.bind(queue).to(exchange);
//    }
}


