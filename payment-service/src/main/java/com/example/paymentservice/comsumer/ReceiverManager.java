package com.example.paymentservice.comsumer;


import com.example.common.constant.OrderStatus;
import com.example.common.constant.PaymentStatus;
import com.example.common.event.OrderEvent;
import com.example.common.event.PaymentEvent;
import com.example.paymentservice.service.PaymentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceiverManager  {
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    ObjectMapper mapper = new ObjectMapper();

    @RabbitListener(queues = "payment-queue")
    public void receiveMessage(String message) throws JsonProcessingException {
        System.out.println("Payment receive message : "+message);
        OrderEvent orderEvent = mapper.readValue(message, OrderEvent.class);
        if(orderEvent.getOrderStatus().equals(OrderStatus.ORDER_CANCELLED)){
            // inventory status REJECTED, rollback payment
            paymentService.cancelOrderEvent(orderEvent);
        }else{
            PaymentEvent paymentEvent = paymentService.newOrderEvent(orderEvent);
            if(paymentEvent.getPaymentStatus().equals(PaymentStatus.RESERVED)){
                //payment success send to inventory service
                orderEvent.getOrderRequest().setPaymentStatus(PaymentStatus.RESERVED);
                rabbitTemplate.convertAndSend("inventory-exchange", "inventory.routing.test", mapper.writeValueAsString(orderEvent));
            }else{
                // payment error
                orderEvent.getOrderRequest().setPaymentStatus(PaymentStatus.REJECTED);
                rabbitTemplate.convertAndSend("order-exchange", "order.routing.test", mapper.writeValueAsString(orderEvent));
            }
        }

    }


}
