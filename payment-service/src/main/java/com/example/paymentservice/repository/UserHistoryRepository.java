package com.example.paymentservice.repository;

import com.example.paymentservice.entity.UserHistory;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserHistoryRepository extends JpaRepository<UserHistory,Long>{
    Optional<UserHistory> findByOrderId(Long id);
}