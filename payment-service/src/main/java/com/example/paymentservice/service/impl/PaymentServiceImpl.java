package com.example.paymentservice.service.impl;

import com.example.common.constant.PaymentStatus;
import com.example.common.dto.OrderRequest;
import com.example.common.dto.PaymentDto;
import com.example.common.event.OrderEvent;
import com.example.common.event.PaymentEvent;
import com.example.paymentservice.entity.UserBalance;
import com.example.paymentservice.entity.UserHistory;
import com.example.paymentservice.repository.UserBalanceRepository;
import com.example.paymentservice.repository.UserHistoryRepository;
import com.example.paymentservice.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PaymentServiceImpl implements PaymentService{
    @Autowired
    private UserBalanceRepository userBalanceRepository;
    @Autowired
    private UserHistoryRepository userHistoryRepository;
    private static final Logger LOG = LoggerFactory.getLogger(PaymentServiceImpl.class);
    @Override
    @Transactional
    public PaymentEvent newOrderEvent(OrderEvent orderEvent) {
        OrderRequest orderRequest = orderEvent.getOrderRequest();
        PaymentDto paymentDto = new PaymentDto(orderRequest.getOrderId(), orderRequest.getUserId(), orderRequest.getTotalPrice());
        try{
            return this.userBalanceRepository.findByUserId(orderRequest.getUserId()).filter(ub->ub.getBalance() >= orderRequest.getTotalPrice())
                    .map(ub->{
                        ub.setBalance(ub.getBalance() - orderRequest.getTotalPrice());
                        UserHistory userHistory = new UserHistory(orderRequest.getUserId(), orderRequest.getOrderId(), orderRequest.getTotalPrice());
                        this.userHistoryRepository.save(userHistory);
                        return new PaymentEvent(paymentDto, PaymentStatus.RESERVED);
                    }).orElse(new PaymentEvent(paymentDto, PaymentStatus.REJECTED));
        }catch (Exception e){
            System.out.println(e.getMessage());
            return new PaymentEvent(paymentDto, PaymentStatus.REJECTED);
        }

    }

    @Override
    @Transactional
    public void cancelOrderEvent(OrderEvent orderEvent) {
        this.userHistoryRepository.findByOrderId(orderEvent.getOrderRequest().getOrderId())
            .ifPresent( ut -> {
                this.userHistoryRepository.delete(ut);
                this.userBalanceRepository.findById(ut.getUserId())
                        .ifPresent(ub -> ub.setBalance(ub.getBalance() + ut.getMount()));
            });
    }

    @Override
    public UserBalance addBanlance(UserBalance userBalance) {
        return userBalanceRepository.save(userBalance);
    }
}
