package com.example.paymentservice.service;

import com.example.common.event.OrderEvent;
import com.example.common.event.PaymentEvent;
import com.example.paymentservice.entity.UserBalance;

public interface PaymentService {
    PaymentEvent newOrderEvent(OrderEvent orderEvent);
    void cancelOrderEvent(OrderEvent orderEvent);
    UserBalance addBanlance(UserBalance userBalance);
}