package com.example.inventoryservice.repository;

import com.example.inventoryservice.entity.ProductConsumed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductConsumedRepository extends JpaRepository<ProductConsumed,Long> {
}
