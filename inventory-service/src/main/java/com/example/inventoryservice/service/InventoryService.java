package com.example.inventoryservice.service;

import com.example.common.event.InventoryEvent;
import com.example.common.event.OrderEvent;
import com.example.inventoryservice.entity.ProductAvailable;

public interface InventoryService {
    InventoryEvent newOrderInventory(OrderEvent orderEvent);
    void cancelOrderInventory(OrderEvent orderEvent);
    ProductAvailable addProduct(ProductAvailable productAvailable);
}
