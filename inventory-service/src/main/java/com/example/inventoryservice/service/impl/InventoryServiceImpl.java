package com.example.inventoryservice.service.impl;

import com.example.common.constant.InventoryStatus;
import com.example.common.dto.InventoryDto;
import com.example.common.dto.OrderDetailDto;
import com.example.common.dto.OrderRequest;
import com.example.common.event.InventoryEvent;
import com.example.common.event.OrderEvent;
import com.example.inventoryservice.entity.ProductAvailable;
import com.example.inventoryservice.entity.ProductConsumed;
import com.example.inventoryservice.repository.ProductAvailableRepository;
import com.example.inventoryservice.repository.ProductConsumedRepository;
import com.example.inventoryservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private ProductAvailableRepository productAvailableRepository;
    @Autowired
    private ProductConsumedRepository productConsumedRepository;
    @Override
    @Transactional
    public InventoryEvent newOrderInventory(OrderEvent orderEvent) {
        InventoryDto inventoryDto = new InventoryDto(orderEvent.getOrderRequest().getOrderId(), orderEvent.getOrderRequest().getOrderDetailSet());
        Set<OrderDetailDto> ls = orderEvent.getOrderRequest().getOrderDetailSet();
        if(validateInventory(ls)){
            return setInventoryEvent(orderEvent.getOrderRequest(), inventoryDto);
        }else{
            return new InventoryEvent(inventoryDto, InventoryStatus.REJECTED);
        }


    }

    protected InventoryEvent setInventoryEvent(OrderRequest orderRequest, InventoryDto inventoryDto){

        for(OrderDetailDto od : orderRequest.getOrderDetailSet()){
            ProductAvailable productAvailable = productAvailableRepository.findById(od.getProductId()).orElse(null);
            if(productAvailable != null){
                productAvailable.setQuantity(productAvailable.getQuantity()-od.getQuantity());
                ProductConsumed productConsumed =  new ProductConsumed(orderRequest.getOrderId(), od.getQuantity());
                productConsumedRepository.save(productConsumed);
                productAvailableRepository.save(productAvailable);
            }else{
                return new InventoryEvent(inventoryDto, InventoryStatus.REJECTED);
            }
        }
        return new InventoryEvent(inventoryDto, InventoryStatus.RESERVED);
    }

    protected  boolean validateInventory(Set<OrderDetailDto> orderDetailDtoSet){
        for(OrderDetailDto od : orderDetailDtoSet){
            ProductAvailable productAvailable = productAvailableRepository.findById(od.getProductId()).filter(i -> i.getQuantity() <= od.getQuantity()).orElse(null);
            if(productAvailable != null){
                return false;
            }
            break;
        }
        return true;
    }

    @Override
    public void cancelOrderInventory(OrderEvent orderEvent) {

    }

    @Override
    public ProductAvailable addProduct(ProductAvailable productAvailable) {
        return productAvailableRepository.save(productAvailable);
    }

}
