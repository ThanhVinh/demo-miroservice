package com.example.inventoryservice.consumer;

import com.example.common.constant.InventoryStatus;
import com.example.common.constant.OrderStatus;
import com.example.common.constant.PaymentStatus;
import com.example.common.event.InventoryEvent;
import com.example.common.event.OrderEvent;
import com.example.inventoryservice.service.InventoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceiverManager implements RabbitListenerConfigurer {
    @Autowired
    private InventoryService inventoryService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    private ObjectMapper mapper = new ObjectMapper();

    @RabbitListener(queues = "inventory-queue")
    public void receiveMessage(String message) throws JsonProcessingException {
        OrderEvent orderEvent = mapper.readValue(message, OrderEvent.class);
        if(orderEvent.getOrderRequest().getPaymentStatus().equals(PaymentStatus.RESERVED) && orderEvent.getOrderStatus().equals(OrderStatus.ORDER_CREATED)){
            InventoryEvent inventoryEvent = inventoryService.newOrderInventory(orderEvent);
            if(inventoryEvent.getInventoryStatus().equals(InventoryStatus.REJECTED)){
                orderEvent.setOrderStatus(OrderStatus.ORDER_CANCELLED);
                orderEvent.getOrderRequest().setPaymentStatus(PaymentStatus.REFUND);
                orderEvent.getOrderRequest().setInventoryStatus(InventoryStatus.REJECTED);
                //send event rollback Payment and Update status order reject
                rabbitTemplate.convertAndSend("order-exchange","order.routing.test", mapper.writeValueAsString(orderEvent));
                rabbitTemplate.convertAndSend("payment-exchange","payment.routing.test", mapper.writeValueAsString(orderEvent));
            }else{
                //update status order reserved, order success;
                orderEvent.setOrderStatus(OrderStatus.ORDER_COMPLETED);
                orderEvent.getOrderRequest().setInventoryStatus(InventoryStatus.RESERVED);
                rabbitTemplate.convertAndSend("order-exchange","order.routing.test", mapper.writeValueAsString(orderEvent));
            }
        }
    }

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {

    }
}
