package com.example.inventoryservice;

import com.example.inventoryservice.entity.ProductAvailable;
import com.example.inventoryservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/inventory")
public class InventoryController {
    @Autowired
    private InventoryService inventoryService;

    @PostMapping("/add/product")
    public ProductAvailable addProduct(@RequestBody ProductAvailable productAvailable){
        return inventoryService.addProduct(productAvailable);
        /**/
    }
}
