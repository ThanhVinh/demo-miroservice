package com.example.inventoryservice.entity;

import javax.persistence.*;

@Entity
@Table(name = "product_consumed")
public class ProductConsumed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_id")
    private Long orderId;

    @Column(name = "quantity_consumed")
    private Integer quantityConsumed;

    public ProductConsumed() {
    }

    public ProductConsumed(Long orderId, Integer quantityConsumed) {
        this.orderId = orderId;
        this.quantityConsumed = quantityConsumed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getQuantityConsumed() {
        return quantityConsumed;
    }

    public void setQuantityConsumed(Integer quantityConsumed) {
        this.quantityConsumed = quantityConsumed;
    }
}
