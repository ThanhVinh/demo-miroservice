package com.example.common.dto;

public class OrderDetailDto {
    private Long id;
    private Long productId;
    private OrderRequest orderDto;
    private String productName;
    private Integer quantity;
    private Double unitPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public OrderRequest getOrderDto() {
        return orderDto;
    }

    public void setOrderDto(OrderRequest orderDto) {
        this.orderDto = orderDto;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public OrderDetailDto() {
    }

    public OrderDetailDto(Long id, Long productId, OrderRequest orderDto, String productName, Integer quantity, Double unitPrice) {
        this.id = id;
        this.productId = productId;
        this.orderDto = orderDto;
        this.productName = productName;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "OrderDetailDto{" +
                "id=" + id +
                ", productId=" + productId +
                ", orderDto=" + orderDto +
                ", productName='" + productName + '\'' +
                ", quantity=" + quantity +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
