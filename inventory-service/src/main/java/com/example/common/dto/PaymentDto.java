package com.example.common.dto;


public class PaymentDto {
    private Long orderId;
    private Long userId;
    private Double amount;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public PaymentDto() {
    }

    public PaymentDto(Long orderId, Long userId, Double amount) {
        this.orderId = orderId;
        this.userId = userId;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PaymentDto{" +
                "orderId=" + orderId +
                ", userId=" + userId +
                ", amount=" + amount +
                '}';
    }
}
