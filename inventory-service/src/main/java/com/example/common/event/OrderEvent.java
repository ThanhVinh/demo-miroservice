package com.example.common.event;

import com.example.common.Event;
import com.example.common.constant.OrderStatus;
import com.example.common.dto.OrderRequest;

import java.util.Date;
import java.util.UUID;

public class OrderEvent implements Event {
    private final UUID eventId = UUID.randomUUID();
    private final Date date = new Date();
    private OrderRequest orderRequest;
    private OrderStatus orderStatus;

    @Override
    public UUID getEventId() {
        return this.eventId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

    public OrderRequest getOrderRequest() {
        return orderRequest;
    }

    public void setOrderRequest(OrderRequest orderRequest) {
        this.orderRequest = orderRequest;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public OrderEvent() {
    }

    public OrderEvent(OrderRequest orderRequest, OrderStatus orderStatus) {
        this.orderRequest = orderRequest;
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return "OrderEvent{" +
                "eventId=" + eventId +
                ", date=" + date +
                ", orderRequest=" + orderRequest +
                ", orderStatus=" + orderStatus +
                '}';
    }
}
